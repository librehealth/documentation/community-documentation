Installation and running with Docker
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. toctree::
   :maxdepth: 1
   :hidden:

   installation-mac
   installation-windows
   installation-fedora
   installation-ubuntu

We have a `repository <https://gitlab.com/librehealth/toolkit/lh-toolkit-docker>`_ which hosts docker compose and resource files required to build docker
images and start containers that can be used by users and developers to test the
latest code in the repository. The Docker container that is created uses standard
`tomcat7:jre8` and `mysql:5.7` images.

The following is included:

* the legacy-ui module
* webservices.rest module
* the owa module
* demo database for Toolkit 2.1 SNAPSHOT

The `tomcat:7-jre8` uses OpenJDK 8, which in turn uses Debian 8.1 (Jessie) base
image. Thus the LH Toolkit is deployed on tomcat7. The MySQL 5.7 image is used
for the database on which the 2.1 SNAPSHOT demo database is deployed. The main
advantage of using standard images is that the updates to those are automatically
available. The LH Toolkit.war is downloaded from the latest devbuilds that are
uploaded to bintray, using Gitlab CI. Thus, the web application built from the
master branch is available as part of this container. You only have to update
the image and get the latest and greatest LH Toolkit that is available from the
Continuous Integration.
