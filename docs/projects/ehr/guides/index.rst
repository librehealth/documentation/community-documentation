LibreHealth EHR Guides
----------------------

.. note:: LibreHealth EHR documentation resides on `their wiki <https://wiki.ehr.librehealth.io/To_The_Docs>`_, which runs MediaWiki.

.. toctree::
   :hidden:
   :maxdepth: 2
   :caption: LibreHealth EHR Guides
