@ECHO OFF

pushd %~dp0

python -m pip install -U pip
python -m pip install -U poetry
python -m poetry install

REM Command file for Sphinx documentation
if "%SPHINXBUILD%" == "" (
	set SPHINXBUILD=python -m poetry run sphinx-autobuild
)
set SOURCEDIR=docs
set BUILDDIR=_build

if "%1" == "" goto help

%SPHINXBUILD% >NUL 2>NUL
if errorlevel 9009 (
	echo.
	echo.The 'sphinx-autobuild' command was not found. Make sure you have Sphinx
	echo.installed, then set the SPHINXBUILD environment variable to point
	echo.to the full path of the 'sphinx-build' executable. Alternatively you
	echo.may add the Sphinx directory to PATH.
	echo.
	echo.If you don't have Sphinx installed, grab it from
	echo.http://sphinx-doc.org/
	exit /b 1
)
python -m poetry run sphinx-autobuild -b html %SOURCEDIR% %BUILDDIR% %SPHINXOPTS%

goto end

:help
python -m poetry run sphinx-autobuild -h

:end
popd
