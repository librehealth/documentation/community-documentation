LibreHealth Toolkit Role Tutorials
-----------------------------------

LibreHealth Toolkit (LH Toolkit) is a complex system with a role-based access control system with various privileges, which dictates what a user can do within the system. This section of our documentation covers how some of the roles that exist by default work, and what each of them can do.

.. toctree::
   :maxdepth: 1
   :caption: Role Tutorials

   superuser/index
   anonymous-and-authenticated/anonymous-authenticated-tutorial
   clinician/clinician-tutorial
   data-assistant/data-assistant-tutorial
   data-manager/data-manager-tutorial
