.. LibreHealth Community documentation master file, created by
   sphinx-quickstart on Wed Mar 14 12:56:56 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

LibreHealth Community Documentation
===================================

Welcome to the LibreHealth documentation site. We are just getting started.

About this documentation
------------------------

We aim for this to be the one-stop for all documentation related to the LibreHealth community, as well as our member projects.

.. toctree::
   :hidden:
   :maxdepth: 2
   :caption: Member Projects Resources

   projects/ehr/index
   projects/radiology/index
   projects/toolkit/index

.. toctree::
   :hidden:
   :maxdepth: 4
   :caption: Development and User Resources

   projects/ehr/guides/index
   projects/radiology/guides/index
   projects/toolkit/guides/index

.. toctree::
   :hidden:
   :maxdepth: 2
   :caption: Community Governance Resources

   governance/governance
   governance/code-of-conduct
   governance/contribution-policy
   governance/copyright-dmca-policy
   governance/trademark-policy
   governance/privacy-policy
