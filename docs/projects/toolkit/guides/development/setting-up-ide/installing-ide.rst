Installing the Integrated Development Environment (IDE)
=======================================================

The project can be cloned into any of the desired folders in the local system using IntelliJ IDEA.
IntelliJ IDEA has a community version that is distributed for free.
You can install it on Ubuntu from Ubuntu Software Center.

.. image:: images/searching-for-intellij.png
   :alt: searching for IntelliJ


Click on the name of the IDE you want to install and than click "Install".


.. image:: images/installing-intellij.png
   :alt: installing IntelliJ


You can launch the IDE when installation process is over.


.. image:: images/launching-ide.png
   :alt: launching ide


.. note:: Please refer to the official `documentation <https://www.jetbrains.com/idea/download/>`_ for installation instructions for other operating systems.

.. note:: You can also use the console (terminal) and any other IDE that supports Java if you are familiar with these tools.
